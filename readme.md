#### Group parent pom

In `maven` a parent pom defines a set of common items that a project should
inherit. It provides a one stop shop towards having all projects under the 
parent to have a base configuration leaving child poms not having to redefine
the same behavior over and over. This repository house the POM for all Java
artifacts under this group.

This pom can be applied to a project by first ensuring you have access to the
`projects4fun.com` maven repository. This repository is password protected to
reduce accidental retrievals. In your maven home `conf/settings.xml` find the 
`<server></server>` tags and add a new server and ensure you keep the same `id`.

    <server>
        <id>internal-proxy-maven-repo</id>
        <username>mavenrepo</username>
        <password>b6105659-5b01-4e93-8d80-af14b5b03538</password>
    </server>

In a newly created maven project you want to define the repository that can be
used to download additional artifacts. This can be defined by adding

    <repositories>
        <repository>
            <id>internal-proxy-maven-repo</id>
            <name>Internal Maven Repo</name>
            <url>https://projects4fun.com/maven2/</url>
        </repository>
    </repositories>

To your pom. It is important to keep the `id` of this project identical to that
which was provided in `server` tags previously.

Finally declare that the `pom.xml` has a parent by adding

    <parent>
        <groupId>com.projects4fun</groupId>
        <artifactId>parent-pom</artifactId>
        <version>1.4.0</version>
    </parent>
    
Your final pom should look like the following.

    <?xml version="1.0" encoding="UTF-8"?>
    <project xmlns="http://maven.apache.org/POM/4.0.0"
             xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
             xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
        <modelVersion>4.0.0</modelVersion>
    
        <parent>
            <groupId>com.projects4fun</groupId>
            <artifactId>parent-pom</artifactId>
            <version>1.1.0</version>
        </parent>
    
        ...
    
        <repositories>
            <repository>
                <id>internal-proxy-maven-repo</id>
                <name>Internal Maven Repo</name>
                <url>https://projects4fun.com/maven2/</url>
            </repository>
        </repositories>
    
        ...
    </project>
