<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>
    <packaging>pom</packaging>

    <groupId>com.projects4fun</groupId>
    <artifactId>parent-pom</artifactId>
    <version>1.4.0</version>
    <inceptionYear>2018</inceptionYear>

    <!-- the parent pom cannot point to its repo or else children incorrectly
     inherit constructed url -->
    <url>https://gitlab.com/projects4fun/</url>

    <properties>
        <skip.unit.tests>false</skip.unit.tests>
        <skip.integration.tests>true</skip.integration.tests>

        <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
        <project.reporting.outputEncoding>UTF-8</project.reporting.outputEncoding>

        <maven.compiler.source>1.8</maven.compiler.source>
        <maven.compiler.target>1.8</maven.compiler.target>

        <maven.build.helper.version>3.0.0</maven.build.helper.version>
        <maven.compiler.plugin.version>3.8.1</maven.compiler.plugin.version>
        <maven.jar.plugin.version>3.2.0</maven.jar.plugin.version>
        <maven.source.plugin.version>3.2.1</maven.source.plugin.version>
        <maven.javadoc.plugin.version>3.1.1</maven.javadoc.plugin.version>
        <maven.versions.plugin.version>2.7</maven.versions.plugin.version>
        <maven.surefire.plugin.version>3.0.0-M4</maven.surefire.plugin.version>
        <maven.failsafe.plugin.version>3.0.0-M4</maven.failsafe.plugin.version>
        <maven.findbugs.plugin.version>3.0.5</maven.findbugs.plugin.version>
        <wagon.ssh.external.version>3.2.0</wagon.ssh.external.version>

        <junit.jupiter.version>5.4.2</junit.jupiter.version>
    </properties>

    <profiles>
        <profile>
            <id>it</id>
            <properties>
                <skip.unit.tests>true</skip.unit.tests>
                <skip.integration.tests>false</skip.integration.tests>
            </properties>
        </profile>
    </profiles>

    <ciManagement>
        <url>https://gitlab.com/projects4fun/${project.artifactId}/pipelines</url>
        <system>gitlab-ci</system>
    </ciManagement>

    <!-- the parent pom cannot point to its repo or else children incorrectly
     inherit scm constructed url -->
    <scm>
        <connection>scm:git:https://gitlab.com/projects4fun/</connection>
        <url>https://gitlab.com/projects4fun/</url>
    </scm>

    <distributionManagement>
        <repository>
            <id>projects4fun-internal-maven-repo</id>
            <name>projects4fun-internal-maven-repo</name>
            <url>scpexe://projects4fun.com:/opt/artifacts/maven2</url>
        </repository>
    </distributionManagement>

    <dependencies>
        <dependency>
            <groupId>org.junit.jupiter</groupId>
            <artifactId>junit-jupiter-engine</artifactId>
            <version>${junit.jupiter.version}</version>
            <scope>test</scope>
        </dependency>
    </dependencies>

    <build>
        <plugins>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-enforcer-plugin</artifactId>
                <version>3.0.0-M3</version>
                <executions>
                    <execution>
                        <id>enforce-maven</id>
                        <goals>
                            <goal>enforce</goal>
                        </goals>
                        <configuration>
                            <rules>
                                <requireMavenVersion>
                                    <version>3.0.5</version>
                                </requireMavenVersion>
                            </rules>
                        </configuration>
                    </execution>
                </executions>
            </plugin>

            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-compiler-plugin</artifactId>
                <version>${maven.compiler.plugin.version}</version>
                <configuration>
                    <source>${maven.compiler.source}</source>
                    <target>${maven.compiler.target}</target>
                    <showWarnings>true</showWarnings>
                    <showDeprecation>true</showDeprecation>
                </configuration>
            </plugin>

            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-jar-plugin</artifactId>
                <version>${maven.jar.plugin.version}</version>
                <configuration>
                    <archive>
                        <manifest>
                            <addDefaultImplementationEntries>true</addDefaultImplementationEntries>
                            <addDefaultSpecificationEntries>true</addDefaultSpecificationEntries>
                        </manifest>
                    </archive>
                </configuration>
            </plugin>

            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-source-plugin</artifactId>
                <version>${maven.source.plugin.version}</version>
                <executions>
                    <execution>
                        <id>attach-sources</id>
                        <goals>
                            <goal>jar</goal>
                        </goals>
                    </execution>
                </executions>
            </plugin>

            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-javadoc-plugin</artifactId>
                <version>${maven.javadoc.plugin.version}</version>
                <executions>
                    <execution>
                        <id>attach-javadocs</id>
                        <goals>
                            <goal>jar</goal>
                        </goals>
                    </execution>
                </executions>
            </plugin>

            <plugin>
                <artifactId>maven-surefire-plugin</artifactId>
                <version>${maven.surefire.plugin.version}</version>
            </plugin>

            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-surefire-report-plugin</artifactId>
                <version>${maven.surefire.plugin.version}</version>
            </plugin>

            <plugin>
                <groupId>org.codehaus.mojo</groupId>
                <artifactId>findbugs-maven-plugin</artifactId>
                <version>${maven.findbugs.plugin.version}</version>
            </plugin>

            <!-- integration test directory support -->
            <plugin>
                <groupId>org.codehaus.mojo</groupId>
                <artifactId>build-helper-maven-plugin</artifactId>
                <version>${maven.build.helper.version}</version>
                <executions>
                    <execution>
                        <id>add-it-sources</id>
                        <phase>generate-test-sources</phase>
                        <goals>
                            <goal>add-test-source</goal>
                        </goals>
                        <configuration>
                            <sources>
                                <source>src/it/java</source>
                            </sources>
                        </configuration>
                    </execution>

                    <execution>
                        <id>add-it-resources</id>
                        <phase>generate-test-resources</phase>
                        <goals>
                            <goal>add-test-resource</goal>
                        </goals>
                        <configuration>
                            <resources>
                                <resource>
                                    <filtering>true</filtering>
                                    <directory>src/it/resources</directory>
                                </resource>
                            </resources>
                        </configuration>
                    </execution>
                </executions>
            </plugin>

            <!-- maven fail safe configuration -->
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-failsafe-plugin</artifactId>
                <version>${maven.failsafe.plugin.version}</version>
                <executions>
                    <execution>
                        <goals>
                            <goal>integration-test</goal>
                            <goal>verify</goal>
                        </goals>
                        <configuration>
                            <skipTests>${skip.integration.tests}</skipTests>
                        </configuration>
                    </execution>
                </executions>
            </plugin>
        </plugins>

        <extensions>
            <extension>
                <groupId>org.apache.maven.wagon</groupId>
                <artifactId>wagon-ssh-external</artifactId>
                <version>${wagon.ssh.external.version}</version>
            </extension>
        </extensions>
    </build>

    <reporting>
        <plugins>
            <plugin>
                <groupId>org.codehaus.mojo</groupId>
                <artifactId>versions-maven-plugin</artifactId>
                <version>${maven.versions.plugin.version}</version>
                <reportSets>
                    <reportSet>
                        <reports>
                            <report>dependency-updates-report</report>
                            <report>plugin-updates-report</report>
                            <report>property-updates-report</report>
                        </reports>
                    </reportSet>
                </reportSets>
            </plugin>

            <plugin>
                <groupId>org.codehaus.mojo</groupId>
                <artifactId>findbugs-maven-plugin</artifactId>
                <version>${maven.findbugs.plugin.version}</version>
            </plugin>
        </plugins>
    </reporting>
</project>
